pipeline {
  agent any

  environment{
    scannerHome = tool 'sonar-scanner'
    def jenkinsUrlJson = "{\"url\":\"${BUILD_URL}\"}"
    def nodeRedUrl = '168.61.222.105:1880'
    def shortCommit = sh(returnStdout: true, script: "git log -n 1 --pretty=format:'%h'").trim()
    docker_credentials = credentials('dockerhub')
    def DOCKER_USER = '$docker_credentials_USR'
    def DOCKER_PASSWORD = '$docker_credentials_PSW'

  }
  post {
    always {
      sh "docker-compose down -v"
      sh "curl --header 'Content-Type: application/json' --request POST --data '${jenkinsUrlJson}' ${nodeRedUrl}/JenkinsBuildResult"
    }
    failure {
      hygieiaDeployPublishStep applicationName: 'cimonitoring', artifactDirectory: '.', artifactGroup: 'cimonitoring', artifactName: '*.txt', artifactVersion: "${shortCommit}", buildStatus: 'Failure', environmentName: 'Prod'
    }
  }
  options {
    gitLabConnection('gitlab-connection')
    gitlabBuilds(builds: ['build', 'lint', 'test', 'deploy'])
    disableConcurrentBuilds()
  }
  triggers {
    gitlab(triggerOnPush: true, triggerOnMergeRequest: false, branchFilterType: 'All')
  }
  stages {
    stage('pre'){      
      steps{
        // Notify Build Started
        sh "curl --header 'Content-Type: application/json' --request POST --data '${jenkinsUrlJson}' ${nodeRedUrl}/JenkinsBuildStarted"
      }
    }
    stage('build'){
      steps{
        gitlabCommitStatus(name: 'build'){
          sh 'docker-compose up -d' // --build'
        }
      }
    }
    stage('lint'){
      steps{
        gitlabCommitStatus(name: 'lint'){
          sh "echo 'Lint stage'"
          sh 'docker-compose run web bundle exec rubocop --rails'
        }
      }
    }
    stage('test'){
      steps{
        gitlabCommitStatus(name: 'test'){
          sh "echo 'Test stage'"
          sh 'docker-compose run web rake db:create db:migrate RAILS_ENV=test spec'
          sh 'mkdir -p reports'
          sh 'cp coverage/.resultset.json reports/.resultset.json'
          sh "sed -i 's+/app/app+${WORKSPACE}/app+g' reports/.resultset.json"
          withSonarQubeEnv('sonarqube-jenkins'){
            sh "${scannerHome}/bin/sonar-scanner"
          }
        }
      }
    }
    stage('deploy'){
      steps{
        gitlabCommitStatus(name: 'deploy'){
          // TODO: upload docker image to docker container registry using commit hash, and deploy using ansible playbook
          sh "echo 'Deploy stage'"
          sh "echo ${DOCKER_PASSWORD} | docker login --username ${DOCKER_USER} --password-stdin"
          sh "docker push merlijnelderhuis/rubydockermerlijn:${shortCommit}"
          // Ansible commands
          // sh "ansible app_servers -m ping -i provision/inventory.ini"
          // sh "ansible-playbook -i provision/inventory.ini playbook.yml -e \"shortCommit=${shortCommit}\""
          // hygieiaDeployPublishStep applicationName: 'cimonitoring', artifactDirectory: '.', artifactGroup: 'cimonitoring', artifactName: 'testfile1.txt', artifactVersion: "${shortCommit}", buildStatus: 'Success', environmentName: 'Development'
          // hygieiaDeployPublishStep applicationName: 'cimonitoring', artifactDirectory: '.', artifactGroup: 'cimonitoring', artifactName: 'testfile1.txt', artifactVersion: "${shortCommit}", buildStatus: 'Success', environmentName: 'Production'
        }
      }
    }
  }
}
